#! /usr/bin/env python
# -*- coding: utf-8 -*-
import ast
import os
from flask import Flask
from flask.ext.restful import Api
from influxdb.influxdb08 import InfluxDBClient
from argus.views import resources
from argus.stats import Stats


def _mask_value(v):
    if isinstance(v, basestring):
        if 'secret' in v.lower() or 'pass' in v.lower():
            return '*' * 8
    return v


def configure_app(app):
    app.config.from_object('argus.config')
    prefix = 'ARGUS_'
    l = len(prefix)
    for k, v in os.environ.iteritems():
        if k.startswith(prefix):
            k = k[l:]
            try:
                v = ast.literal_eval(v)
            except (ValueError, SyntaxError):
                pass
            app.config[k] = v
            app.logger.debug(
                'overriding config by env: %s to %s', k, _mask_value(v))


def configure_extensions(app):
    app.influxdb = InfluxDBClient(
        host=app.config['INFLUXDB_HOST'],
        port=app.config['INFLUXDB_PORT'],
        username=app.config['INFLUXDB_USERNAME'],
        password=app.config['INFLUXDB_PASSWORD'],
        database=app.config['INFLUXDB_DATABASE'],
    )
    api = Api(app, prefix='/api')
    api.add_resource(resources.Points, '/point/')
    api.add_resource(resources.TimedPoints, '/point/timed/')
    Stats(app)


def configure_views(app):
    @app.route('/')
    def index():
        return 'hello world'


def create_app():
    app = Flask('argus')
    configure_app(app)
    configure_extensions(app)
    configure_views(app)

    return app


if __name__ == '__main__':
    create_app().run(host='0.0.0.0', port=8000, debug=True)
