#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time
from flask import g, request
from argus.utils import collect_stats


class Stats(object):
    def __init__(self, app=None):
        self.app = app
        if app:
            self.init_app(app)

    def init_app(self, app):
        app.before_request(self.stats_before_request)
        app.after_request(self.stats_after_request)

    @staticmethod
    def _meta(req):
        return {
            'method': req.method,
            'path': req.path,
        }

    def stats_before_request(self):
        g.start = time.time()
        meta = self._meta(request)
        collect_stats('request_count', 1, meta)
        collect_stats('request_size', request.content_length or 0, meta)

    def stats_after_request(self, resp):
        t = time.time() - g.start
        meta = self._meta(request)
        collect_stats('response_time', t, meta)
        collect_stats('response_size', resp.content_length or 0, meta)
        return resp


