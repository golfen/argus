#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import current_app


def collect_stats(name, value, tags=None):
    if not tags:
        tags = {}
    tags['value'] = value
    columns, points = zip(*tags.items())
    current_app.influxdb.write_points([{
        'name': name,
        'columns': columns,
        'points': [points]
    }])
