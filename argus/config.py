#!/usr/bin/env python
# -*- coding: utf-8 -*-
DEBUG = False

# influxdb
INFLUXDB_HOST = '127.0.0.1'
INFLUXDB_PORT = 8086
INFLUXDB_USERNAME = 'root'
INFLUXDB_PASSWORD = 'root'
INFLUXDB_DATABASE = 'argus'
