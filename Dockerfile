FROM python:2.7.7
MAINTAINER blurrcat <blurrcat@gmail.com>
# building gevent takes a hell lot of time; cache it here
RUN mkdir /usr/src/app && pip install -U pip && pip install gevent
WORKDIR /usr/src/app

ENV WEB_CONCURRENCY=1 \
    ARGUS_DEBUG=False
EXPOSE 8000

ADD requirements.txt ./
RUN pip install -r requirements.txt
ADD . .
RUN pip install -e .

CMD gunicorn -b 0.0.0.0:8000 -k gevent --access-logfile "-" \
    --access-logformat '[%(t)s]%(h)s %(l)s "%(r)s" %(s)s %(b)s "%(f)s" "%(a)s"' \
    "argus.main:create_app()"
