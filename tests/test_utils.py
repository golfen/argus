#!/usr/bin/env python
# -*- coding: utf-8 -*-
from argus.utils import collect_stats


def test_collect_stats(app):
    collect_stats('collect', 10)
    collect_stats('collect_tags', 10, {'host': '10.0.0.0'})
