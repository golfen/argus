#!/usr/bin/env python
# -*- coding: utf-8 -*-


def test_timed_points(client, tags, timed_points):
    r = client.post('/api/point/timed/', json={
        'tags': tags,
        'points': timed_points,
    })
    assert r.status_code == 201, r.data


def test_points(client, tags, points):
    r = client.post('/api/point/', json={
        'tags': tags,
        'points': points,
    })
    assert r.status_code == 201, r.data


def test_bad_requests(client, tags):
    # missing timestamp
    r = client.post('/api/point/timed/', json={
        'tags': tags,
        'points': [{
            'name': 'test',
            'value': 10,
        }]
    })
    assert r.status_code == 400
    assert 'Required parameter "timestamp" not found' in r.json['message']

    # bad timestamp
    r = client.post('/api/point/timed/', json={
        'tags': tags,
        'points': [{
            'name': 'test',
            'value': 10,
            'timestamp': 10,
        }]
    })
    assert r.status_code == 400
    assert r.json['message'] == (
        'Expected type "datetime" for timestamp, got "integer"')

    # bad timestamp
    for t in ('2014', 20141011):
        r = client.post('/api/point/timed/', json={
            'tags': tags,
            'points': [{
                'name': 'test',
                'value': 10,
                'timestamp': t,
            }]
        })
        assert r.status_code == 400
        assert 'Expected type "datetime"' in r.json['message']
